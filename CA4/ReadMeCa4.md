CA4 - Containers with Docker
-------------

Install docker tollbox.

Download "https://github.com/atb/docker-compose-demo".
Place errai-demonstration-gradle.war inside web-folder


     docker-compose build
	 docker-compose up
	 
On web folder dockerfile add:

     && \\
	 sed -i 's\#\<connection-url\>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE\</connection-url\>\#\<connection-url\>jdbc:h2:file://usr/src/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE\</connection-url\>\#g'
     /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \\
     sed -i 's\#\<password\>sa\</password\>\#\<password\>\</password\>\#g'
     /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
     COPY /errai-demonstration-gradle.war
     /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/
	 
Edit docker-compose.yml,
Changed the db to be first and the web second. So it can execute the web already with the initiated db. 
	 	 
After all these steps I executed the command

     docker-compose build
	 docker-compose up

Insert in dockerfile:
    
	 sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' 

After doing the docker-compose build and the docker-compose up in H2 page I put in the JBDC URL:
 
     jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
	 
	 
Then to publish the images:

Create DockerHub account - 1160240.

     docker login 

Login with the dockerHub credentials

Then to check my images:

     docker images

Now tag the latests

	docker tag local-image:tagname reponame:tagname
    docker push reponame:tagname
	

Unfortunely could not push them to dockerHub repository as when I was tryng to push them the error I got was:

denied: requested access to the resource is denied

![error](https://i.gyazo.com/32348221ccfc5f7fe0e05ff7f58a4cd4.png)
CA3, Part 1 - Virtualization with Vagrant

Before doing Class Assignment 3 - Part1, I started by installing VirtualBox on my system. Once installed, I also downloaded the ISO file Ubuntu 18.04, and installed it in VirtualBox. After having installed all the necessary tools for this operation, I clone the errai project and I ran some commands:

	 ./gradlew provision
	 ./gradlew build
     ./gradlew startWildfly
	 

As soon the VM and gradle was set up, I could access ''http://192.168.56.100:8080/errai-demonstration-gradle" perfectly.


Before cloning my repository to the VM I removed CA6 temporarily.

Now clone my repo to VM :

git clone https://Ivo_Guerra@bitbucket.org/Ivo_Guerra/devops-18-19-atb-1160240.git 

Go to CA1 folder and run Maven:
 mvn gwt:run
 
 Needs maven installed:
 
 sudo apt install maven
 
 
Then tried:

	./gradlew build
		
Error:	
	 Failed to execute goal on project errai-demonstration : Could not resolve dependencies for project org.atb.errai.demos:errai-demonstration:war:1.1.0-SNAPSHOT: Could not find artifact sun.jdk:jconsole:jar:jdk at specified path /usr/lib/jvm/java-8-jdk-amd64/jre/../lib/jconsole.jar
This error happens because Virtual Machine has no graphic interface and can not build with maven

Then go to CA2 install gradle:

sudo apt install gradle

Got error with task 'taskZip' so commented it,

gradle build

After the build was done, I then proceeded to the execution of the chat, with the following command:

     gradle executeServer
	 
On my machine in CA2 Part1 go to the build.gradle file and on 'task runClient' 
	change 'localhost' to --> '192.168.56.100'

Then on the command line:
	
	 ./gradlew runClient

And POP, chat open and server recalls the chat info!


CA3, Part 2 - Virtualization with Vagrant

download https://github.com/atb/vagrant-multi-demo

Build the war of the errai-demonstration-gradle and copy the file into the previous folder
3

Execute vagrant up


Add config.vm.synced_folder to syncfolders:
	config.vm.synced_folder "/DiretorioDevops/devops-18-19-atb-1160240/CA3/Part2/vagrant-multi-demo-master/sharedfolder" , "/vagrant"

	
	
Updated web.vm.provision  to:
		
		web.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # We assume that errai-demonstration-gradle.war is located in the host folder that contains the Vagrantfile
      sudo cp /vagrant/errai-demonstration-gradle.war /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war
      # So that the wildfly server always starts
      sudo nohup sh /home/vagrant/wildfly-15.0.1.Final/bin/standalone.sh > /dev/null &
	
	  sudo rm errai-demonstration-gradle.war.failed
	  sed -i 's#<connection-url>jdbc:h2:file:C:/DiretorioDevops/devops-18-19-atb-1160240/CA3/Part2/vagrant-multi-demo-master/sharedfolder</connection-url>#<connection-url>jdbc:h2:tcp://192.168.33.11:9092/~/test</connection-url>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
	  sed -i 's#<password>sa</password>#<password></password>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml   
   SHELL
   
   

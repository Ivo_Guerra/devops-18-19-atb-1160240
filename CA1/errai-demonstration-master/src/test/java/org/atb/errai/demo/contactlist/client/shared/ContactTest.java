package org.atb.errai.demo.contactlist.client.shared;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContactTest {

    @Test
    public void getPhoneNumber() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "912345678";
        String result = contact.getPhoneNumber();

        assertEquals(expectedResult, result);

    }

    @Test
    public void setPhoneNumber() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "913572468";

        contact.setPhoneNumber(expectedResult);

        String result = contact.getPhoneNumber();

        assertEquals(expectedResult, result);

    }

    @Test
    public void getName() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "Johny";
        String result = contact.getName();

        assertEquals(expectedResult, result);
    }

    @Test
    public void setName() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "Carly";

        contact.setName(expectedResult);

        String result = contact.getName();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getEmail() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "johny@hotmail.com";
        String result = contact.getEmail();

        assertEquals(expectedResult, result);
    }

    @Test
    public void setEmail() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "johny@gmail.com";

        contact.setEmail(expectedResult);

        String result = contact.getEmail();

        assertEquals(expectedResult, result);
    }

    @Test
    public void setEmailInvalid() {
        Contact contact = new Contact("Johny", "johny@hotmail.com", "912345678");

        String expectedResult = "This e-mail is invalid!";

        contact.setEmail("johnygmail.com");

        String result = contact.getEmail();

        assertEquals(expectedResult, result);
    }


}